### filter
```
filter() 方法创建给定数组一部分的浅拷贝，其包含通过所提供函数实现的测试的所有元素。
filter() 方法是一个复制方法。它不会改变 this，而是返回一个包含与原始数组相同的元素（其中某些元素已被过滤掉）的浅拷贝。
使用 filter() 创建一个过滤数组，该数组删除了所有值小于 10 的元素。
function isBigEnough(value) {
  return value >= 10;
}

const filtered = [12, 5, 8, 130, 44].filter(isBigEnough);
```
### sort
```
sort()会按照升序重新排列数组元素，即最小的值在前面，最大的值在后面
let values = [0,1,5,10,15]
values.sort();
console.log(values);//0,1,10,15,5
```